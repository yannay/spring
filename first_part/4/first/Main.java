package first;
import java.io.*;

/*
 1. Измените IOFacade.java, чтобы записать объект, используя 
 сериализацию объекта в дополнении к записи в текстовый и двоичный файлы.  
 */

public class Main {
	public static void main(String[] args) throws IOException {
		//IOFacade facade = new IOFacade();
		//facade.writeToTextFile("test.txt", "test_text");
		IOFacade facade = IOFacade.getInstance();
		facade.writeToTextFile("test.txt", "test_before_serialize");
		facade.writeToBinaryFile("test.bin", "test_before_serialize\n".getBytes());

		// test serialization
		try(ObjectInputStream oos = new ObjectInputStream(new FileInputStream("first/serialize.out")))
		{
			IOFacade facade2 = (IOFacade) oos.readObject();
			facade2.writeToTextFile("test.txt", "test_after_deserialize");
			facade.writeToBinaryFile("test.bin", "test_after_deserialize\n".getBytes());
		}
		catch(Exception ex){             
			System.out.println(ex.getMessage());
		}
		
		

	}
}
