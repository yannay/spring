package first;
// File: IOFacade.java
import java.io.*;

public class IOFacade implements Serializable {
  	private static IOFacade theInstance = null;

	private static String path = "first/";

  	public IOFacade() {  

  	}

  	public static IOFacade getInstance() {
		if (theInstance == null) {
		  theInstance  = new IOFacade();
		}

		// serialization into file
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path+"serialize.out")))
        {
            oos.writeObject(theInstance);
        }
        catch(Exception ex){             
            System.out.println(ex.getMessage());
        } 

		return theInstance;
  	}

	public void writeToTextFile(String filename, String text) throws IOException { 
		PrintWriter out = null;
		boolean append = true;
		try {
		  File file = new File(path+filename);
		  out = new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
		  out.println(text);
		}
		catch (IOException e) {
			log(e); 
			throw e; 
		} 
		finally {
			out.close();
		} 
	} 

	public void writeToBinaryFile(String filename, byte[] bytes) throws IOException { 
		BufferedOutputStream out = null;
		boolean append = true;
		try {
		  	File file = new File(path+filename);
			out = new BufferedOutputStream(new FileOutputStream(file, append));
		  	out.write(bytes);
		}
		catch (IOException e) {
		  	log(e);
			throw e; 
		} 
		finally {
		  	try {
				out.close();
		  	}
		  	catch (IOException ioe) {
				log(ioe);
				throw ioe;
			} 	
		} 
  	}

  	private void log(Exception e) {
		try {
			writeToTextFile("log.txt", e.toString()); 
		}
		catch (IOException ioe) {
	  		System.err.println(ioe);
		}
	} 
} 
