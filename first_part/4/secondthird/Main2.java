package secondthird;

/*
2.	GenericArrayAdapter.java из каталога m2 не завершен и выдает исключение UnsupportedOperationException, 
если клиент вызывает методы remove() или removeAll(). Добавьте неободимую функциональность к этим методам, 
используя GenericArray в качестве адаптируемого. Протестируйте методы remove() и removeAll() после добавления. 
 */

public class Main2 {

	public static void main(String[] args) {

		GenericArrayAdapter<Double> adapter = new GenericArrayAdapter<Double>(10);

		// test removeAll
		System.out.println("\ntest removeAll:");
		adapter.add(3.14);
		adapter.add(3.14);
		adapter.add(3.14);
		System.out.println("Size before removeAll: " + adapter.size());
		assert adapter.size() == 3;
		adapter.removeAll(adapter);
		System.out.println("Size after removeAll: " + adapter.size());
		assert adapter.size() == 0;

		// test removeAll for empty Array
		System.out.println("\ntest removeAll for empty Array:");
		adapter.removeAll(adapter);
		System.out.println("Size after removeAll for empty Array: " + adapter.size());
		assert adapter.size() == 0;

		// test remove
		System.out.println("\ntest remove:");
		adapter.add(3.14);
		adapter.add(3.15);
		System.out.println("Size before remove: " + adapter.size());
		assert adapter.size() == 2;
		adapter.remove(3.14);
		System.out.println("Size after remove: " + adapter.size());
		assert adapter.size() == 1;

		// test remove with wrong object
		System.out.println("\ntest remove with wrong object:");
		adapter.remove(7.12);
		System.out.println("Size after removing wrong object: " + adapter.size());
		assert adapter.size() == 1;
	
	}
	
}
