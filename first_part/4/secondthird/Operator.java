// File: Operator.java
package secondthird;

import java.util.ArrayList;
import java.util.Iterator;

public class Operator extends ArithmeticComponent {
  private ArrayList<ArithmeticComponent> children;
  private String operator;

  public Operator(String operator) {
    this.operator = operator;
    children = new ArrayList<ArithmeticComponent>();
  }
  
  public void add(ArithmeticComponent component) {
    children.add(component);
  }
  
  public void remove(ArithmeticComponent component) {
    children.remove(component);
  }

  public ArithmeticComponent getChild(int i) {
    return children.get(i);
  }
  
  public int evaluate() {
    int result = 0;
    boolean firstChild = true;
    Iterator<ArithmeticComponent> it = children.iterator();
    while (it.hasNext()) {
      ArithmeticComponent comp = it.next();

      if (firstChild == true) {
        result = comp.evaluate();
        firstChild = false;
        continue;
      }
      if (operator.equals("+"))
        result += comp.evaluate();
      else if (operator.equals("-"))
        result -= comp.evaluate();
      else if (operator.equals("*"))
        result *= comp.evaluate();
      else if (operator.equals("/"))
        result /= comp.evaluate();
      else {
        System.out.println("Wrong operator. Operand will be skipped.");
      }
    }
    return result;
  }

  protected String printCalc(){
    String result = "";
    for (int i = 0; i < children.size(); i++){
      ArithmeticComponent elem = children.get(i);
      if (elem.getClass().getName().contains("Operand")){
        if (i>0){
          result += this.operator;
        }
        result += elem.evaluate();
      }
      else if (elem.getClass().getName().contains("Operator")){
        result += elem.printCalc(); 
      }
    }
    return result;
  }

  public String print(){
    String result = this.printCalc()+"="+this.evaluate();
    System.out.println(result);
    return result;
  }
}