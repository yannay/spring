package secondthird;

/*
3.	Добавьте функцию print в составной пример, ArithmeticComponent.java(Operator.java, Operand.java) из каталога m2, 
чтобы вычисляемое выражение можно было отобразить на экране. Напишите приложение, которое тестирует добавленную функциональность. 
 */

public class Main3 {

	public static void main(String[] args) {

		// test one operator
		System.out.println("\ntest one operator:");
		Operator operator = new Operator("+");
		Operand operand = new Operand(1);
		Operand operand2 = new Operand(3);
		operator.add(operand);
		operator.add(operand2);
		assert operator.print().equals("1+3=4");

		// test one operand
		System.out.println("\ntest one operand:");
		operator = new Operator("+");
		operand = new Operand(1);
		operator.add(operand);
		assert operator.print().equals("1=1");

		// test multiple operands and operators
		System.out.println("\ntest multiple operands and operators:");
		operator = new Operator("+");
		operand = new Operand(1);
		operand2 = new Operand(3);
		Operand operand3 = new Operand(4);
		operator.add(operand);
		operator.add(operand2);
		operator.add(operand3);
		assert operator.print().equals("1+3+4=8");

		Operator operator2 = new Operator("-");
		Operand operand7 = new Operand(2);
		Operand operand8 = new Operand(5);
		operator2.add(operator);
		operator2.add(operand7);
		operator2.add(operand8);
		assert operator2.print().equals("1+3+4-2-5=1");

		// test wrong operand
		System.out.println("\ntest wrong operand:");
		operator = new Operator("(");
		operand = new Operand(1);
		operand2 = new Operand(3);
		operator.add(operand);
		operator.add(operand2);
		assert operator.print().equals("1(3=1");
	}
	
}
