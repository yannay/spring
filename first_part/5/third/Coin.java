package third;

public class Coin {

	private int nominal;
	private int count = 0;

	public Coin(int nominal){
		this.nominal = nominal;
	}

	public Boolean add(int number){
		if (this.nominal == number){
			count+=1;
			return true;
		}
		else{
			return false;
		}
	}

	public int getCount(){
		return count;
	}

	public int getNominal(){
		return nominal;
	}
	
}
