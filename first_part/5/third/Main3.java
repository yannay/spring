package third;

import java.util.ArrayList;
import java.util.Collections;

/*
3.	Есть 4 приемника монет, каждый из которых принимает монеты только одного номинала (1, 5, 10 и 25 центов). 
Необходимо вывести на экран общую внесенную сумму и количество внесенных монет каждого номинала. 
 */

public class Main3 {
	public static void main(String[] args) {
		int amount = 20;

		// set nominals
		ArrayList<Integer> list = new ArrayList<Integer>();
		Collections.addAll(list, 1, 5, 10, 25);
		int listSize = list.size();

		// set automats
		Coin coins [] = new Coin[listSize];
		for (int i = 0; i < listSize; i++){
			coins[i] = new Coin(list.get(i));
		}

		// fill in automats
		int total = 0;
		ArrayList<Integer> list2 = new ArrayList<Integer>();
		for (int i = 0; i < amount; i++){
			int coin_n = (int)( Math.random() * listSize);
			int coin_nominal = list.get(coin_n);
			list2.add(coin_nominal);
			//System.out.println(coin_nominal);
			total += coin_nominal;
			for (int j = 0; j < listSize; j++){
				if (coins[j].add(coin_nominal)) break;
			}	
		}

		//print results
		System.out.println("Внесенные монеты:");
		System.out.println(list2);
		for (int i = 0; i < listSize; i++){
			System.out.println("Было внесено " + coins[i].getCount() + " монет с номиналом " + coins[i].getNominal());
		}
		System.out.println("Общая внесенная сумма равна " + total);

	}
}
