## **Запуск 1 пункта:**
* javac first/*.java
* java first/Main
## **Запуск 2 пункта:**
* mvn install
* mvn spring-boot:run
## **Запуск 3 пункта:**
* javac third/*.java
* java third/Main3
