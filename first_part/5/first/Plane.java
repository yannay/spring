package first;

public class Plane extends Thread{

	private long runwayTime = 3000/2;
	private long terminalTime = 3000/2;
	private long requestPeriod = 1000/2;

	public int id;
	private Runway r[];
	private Terminal t[];

	public Plane(int id){
		this.id=id;
	}

	public void setRunway(Runway r[]){
		this.r = r;
	}

	public void setTerminal(Terminal t[]){
		this.t = t;
	}

	@Override
	public void run() {
		// landing
		Runway runway = getUsedRunway(this);
		//Runway runway = this.runway;
		if(runway==null){
			System.out.println("plane number "+id+" wants to land");
			//Runway availableRunway = getAvailableRunway();
			while ((runway=atomicallyAttempToLandPlane(this, true))==null) {
				System.out.println("no runway available yet for plane number " + id);
				planeSleep(requestPeriod);
			}
			System.out.println("plane number "+id+" is landing on the runway number "+runway.id);
		}
		planeSleep(runwayTime);
		System.out.println("plane number "+id+" is ready to leave runway number "+runway.id);
		//runway.planeAtomicReference.set(null);
		//System.out.println("plane number "+id+" lefted the runway number "+runway.id);

		// terminal
		Terminal terminal = getUsedTerminal(this);
		if(terminal==null){
			System.out.println("plane number "+id+" wants to go to the terminal");
			//Terminal availableTerminal = getAvailableTerminal();
			while ((terminal=atomicallyAttempToGetToTerminalPlane(this))==null) {
				System.out.println("no terminal available yet for plane number " + id);
				planeSleep(requestPeriod);
			}
			runway.planeAtomicReference.set(null);
			System.out.println("plane number "+id+" lefted the runway number "+runway.id);
			System.out.println("plane number "+id+" is going to the terminal number "+terminal.id);
		}
		planeSleep(terminalTime);
		System.out.println("plane number "+id+" is ready to leave the terminal number "+terminal.id);
		//terminal.planeAtomicReference.set(null);
		//System.out.println("plane number "+id+" lefted the terminal number "+terminal.id);

		// take off
		runway = getUsedRunway(this);
		if(runway==null){
			System.out.println("plane number "+id+" wants to take off");
			while ((runway=atomicallyAttempToLandPlane(this, false))==null) {
				System.out.println("no runway available yet for plane number " + id);
				planeSleep(requestPeriod);
			}
			terminal.planeAtomicReference.set(null);
			System.out.println("plane number "+id+" lefted the terminal number "+terminal.id);
			System.out.println("plane number "+id+" is going to the runway number "+runway.id);
		}
		planeSleep(runwayTime);
		runway.planeAtomicReference.set(null);
		System.out.println("plane number "+id+" ready to leave runway number "+runway.id);
		System.out.println("plane number "+id+" taked off from the runway number "+runway.id);
	}

	// waiting
	private void planeSleep(long time){
		try {
			sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 *
	 * @param plane
	 * @return
	 */
	private Runway atomicallyAttempToLandPlane(Plane plane, Boolean land) {
		int lenMy = (land == true) ? r.length - 1 : r.length;
		for (int i = 0; i < lenMy; i++) {
			if(r[i].planeAtomicReference.compareAndSet(null,plane)){
				return r[i];
			}
		}
		return null;
	}

	// private Runway getAvailableRunway(){
    //     for (int i = 0; i < r.length; i++) {
    //         if(r[i].planeAtomicReference.get() ==null){
    //             return r[i];
    //         }
    //     }
    //     return null;
    // }

	private Runway getUsedRunway(Plane plane){
        for (int i = 0; i < r.length; i++) {
            final Plane planeOnRunway  = r[i].planeAtomicReference.get();
            if(planeOnRunway !=null && planeOnRunway.id==plane.id){
                return r[i];
            }
        }
        return null;
    }

	private Terminal atomicallyAttempToGetToTerminalPlane(Plane plane) {
		for (int i = 0; i < t.length; i++) {
			if(t[i].planeAtomicReference.compareAndSet(null,plane)){
				return t[i];
			}
		}
		return null;
	}


	private Terminal getUsedTerminal(Plane plane){
        for (int i = 0; i < t.length; i++) {
            final Plane planeInTerminal  = t[i].planeAtomicReference.get();
            if(planeInTerminal !=null && planeInTerminal.id==plane.id){
                return t[i];
            }
        }
        return null;
    }

	
}
