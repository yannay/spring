package first;

import java.util.concurrent.atomic.AtomicReference;

public class Terminal {

	public int id;
	public AtomicReference<Plane> planeAtomicReference;

	public Terminal(int i, Plane p) {
		id = i;
		planeAtomicReference = new AtomicReference<>();
		planeAtomicReference.set(p);
	}
}