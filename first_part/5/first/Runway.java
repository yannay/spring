package first;

import java.util.concurrent.atomic.AtomicReference;

public class Runway{

	//public int count = 5;
	//public boolean status = true;
	public int id;
	public AtomicReference<Plane> planeAtomicReference;

	public Runway(int id, Plane p) {
		this.id = id;
		this.planeAtomicReference = new AtomicReference<>();
		planeAtomicReference.set(p);
	}

	// public void changeStatus(long time) throws InterruptedException{
	// 	status = !status;
	// 	Thread.sleep(time);
	// 	status = !status;
	// }
	
}
