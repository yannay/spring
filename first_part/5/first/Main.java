package first;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/*
 1.	Реализовать приложение, которое контролирует движение самолетов с авиадиспетчерской вышки. 
 Сами самолеты напрямую друг с другом не взаимодействуют. Должно быть минимум 3 взлетно-посадочных полосы и 5 терминалов, 
 одновременно на каждой полосе и у каждого терминала находиться только один самолет. 
 */

public class Main {


	static Runway r[] = new Runway[3];
	static Terminal t[] = new Terminal[5];
	static Plane p[] = new Plane[5];

	public static void main(String[] args) {

		// create planes
		for (int i = 0; i < p.length; i++){
            p[i] = new Plane(i);
        }

		// set runways and terminals
		List<Plane> planes = Arrays.asList(p);
        Iterator<Plane> iterator;
        Collections.shuffle(planes);
        iterator= planes.iterator();
        for (int i = 0; i < r.length; i++) {
            Plane p;
            try {
                p= iterator.next();
            }catch ( RuntimeException e){
                p= null;
            }
            r[i] = new Runway(i,p);
        }
		for (int i = 0; i < t.length; i++) {
			t[i] = new Terminal(i,null);
		}

		//display initial state
        for (int i = 0; i < p.length; i++){
            Runway runway = getUsedRunway(p[i]);
            System.out.println("plane number "+p[i].id + " is "+(runway==null?"waiting for landing":("on the runway number "+runway.id)));
        }

        System.out.println("======== Begin! ============");

        // start landing
        for (int i = 0; i < p.length; i++){
			p[i].setTerminal(t);
			p[i].setRunway(r);
            p[i].start();
        }

	}

	private static Runway getUsedRunway(Plane plane){
        for (int i = 0; i < r.length; i++) {
            final Plane planeOnRunway  = r[i].planeAtomicReference.get();
            if(planeOnRunway !=null && planeOnRunway.id==plane.id){
                return r[i];
            }
        }
        return null;
    }
}