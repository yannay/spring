package second.courses.controller;

import net.rgielen.fxweaver.core.FxControllerAndView;
import net.rgielen.fxweaver.core.FxmlView;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.stage.Stage;

import second.courses.service.CodesService;

@Component
@FxmlView("main-stage.fxml")
public class MainController {

	private Stage stage;
	private final FxControllerAndView<CurrencyController, VBox> currencyDialog;

	@FXML private Label mainMenuLabel;
	@FXML private TextField txtDateStart;
    @FXML private TextField txtDateFinish;
	@FXML private ComboBox<String> cbCurrency;
	@FXML private VBox menu;

	@FXML public Button openCurrencyButton;

	//@Autowired
    public MainController(FxControllerAndView<CurrencyController, VBox> currencyDialog) {
		//public MainController(){
		this.currencyDialog = currencyDialog;
        
    }

	@FXML
	public void initialize() {
		// this.stage = new Stage();
        // stage.setScene(new Scene(menu));
		List<List<String>> dataCodes = this.getCodes();
		ObservableList<String> curencyCodes = FXCollections.observableArrayList(dataCodes.get(0));
		//this.getCodes();
		cbCurrency.setItems(curencyCodes);
		//cbCurrency.setValue("---");

		//stage.show();

		openCurrencyButton.setOnAction( (actionEvent )-> {

					String dateStart = txtDateStart.getText();
					String dateFinish = txtDateFinish.getText();
					String currencyValue = cbCurrency.getValue();
					String currencyCode = "";

					if (!StringUtils.hasLength(dateStart) || !StringUtils.hasLength(dateFinish) || !StringUtils.hasLength(currencyValue) ) {
						System.out.println("Empty fields");
						return;
					}

					for (int i = 0; i < dataCodes.get(0).size(); i++){
						if (dataCodes.get(0).get(i).equals(currencyValue)){
							//System.out.println(i);
							currencyCode = dataCodes.get(1).get(i).toString().trim();
							if (!StringUtils.hasLength(currencyCode)) {
								System.out.println("Bad currency");
								return;
							}
							break;
						}
					}
					//System.out.println(currencyCode);

					currencyDialog.getController().show(dateStart, dateFinish, currencyValue, currencyCode);
				}
			);

	}

	private List<List<String>> getCodes() {
		CodesService codesService = new CodesService();
		return codesService.getCodes();

	}
	
}
