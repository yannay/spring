package second.courses.controller;

import java.util.List;

import org.springframework.stereotype.Component;

import net.rgielen.fxweaver.core.FxmlView;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import second.courses.service.CurrencyService;


@Component
@FxmlView("get_currency.fxml")
public class CurrencyController {

	private Stage stage;

	@FXML private VBox getCurrency;


	@FXML
	public void initialize() {
		this.stage = new Stage();
        //stage.setScene(new Scene(getCurrency));

	}


	public void show(String dateStart, String dateFinish, String currencyValue, String currencyCode) {
		List<List<String>> dataList = this.getCurrency(dateStart, dateFinish, currencyCode);
        CategoryAxis x = new CategoryAxis();
        NumberAxis y = new NumberAxis();

        LineChart<String, Number> numberLineChart = new LineChart<String, Number>(x,y);
        numberLineChart.setTitle("Course of the " + currencyValue);
        XYChart.Series series1 = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        //series2.setName("cos(x)");
        series1.setName(currencyValue);
        ObservableList<XYChart.Data> datas = FXCollections.observableArrayList();
        //ObservableList<XYChart.Data> datas2 = FXCollections.observableArrayList();
        for(int i=0; i<dataList.get(0).size(); i++){
			Double value  = Double.parseDouble(dataList.get(1).get(i).replaceAll(",","."));
			String date = dataList.get(0).get(i);
			datas.add(new XYChart.Data(date, value));
            //datas2.add(new XYChart.Data(i,Math.cos(i)));
        }

        series1.setData(datas);
        //series2.setData(datas2);

        Scene scene = new Scene(numberLineChart, 600,600);
        numberLineChart.getData().add(series1);
        //numberLineChart.getData().add(series2);
        stage.setScene(scene);

        stage.show();
    }

	private List<List<String>> getCurrency(String dateStart, String dateFinish, String currencyCode) {
		CurrencyService currencyService = new CurrencyService();
		return currencyService.getCurrency(dateStart, dateFinish, currencyCode);

	}
	
}
