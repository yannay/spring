package second.courses;

import java.io.IOException;
import java.net.URL;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import net.rgielen.fxweaver.core.FxWeaver;

import second.courses.controller.MainController;

public class JavaFxApplication extends Application {

    private static Scene scene;
	private ConfigurableApplicationContext applicationContext;
	private static FxWeaver fxWeaver;
	private static Parent root;

    @Override
    public void init() {
        String[] args = getParameters().getRaw().toArray(new String[0]);

        this.applicationContext = new SpringApplicationBuilder()
                .sources(Main2.class)
                .run(args);
    }

	@Override
	public void start(Stage stage) {
	    fxWeaver = applicationContext.getBean(FxWeaver.class);
	    root = fxWeaver.loadView(MainController.class);
	    scene = new Scene(root);
	    stage.setScene(scene);
	    stage.show();
	}

	public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

	// public static void setRoot2(Class controller) throws IOException {
    //     root = fxWeaver.loadView(controller);
	// 	scene = new Scene(root);
    // }

	private static Parent loadFXML(String fxml) throws IOException {
		//if (fxml == "main-stage"){
			fxml = "second/courses/controller/" + fxml;
		//}
		URL url = JavaFxApplication.class.getClassLoader().getResource(fxml + ".fxml");
        //System.out.println("\ngetResource : " + url);
		FXMLLoader fxmlLoader = new FXMLLoader(url); //  /com/example/crudjavafx/controller
        return fxmlLoader.load();
    }

	@Override
	public void stop() {
	    this.applicationContext.close();
	    Platform.exit();
	}

}
