package second.courses.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

@Service
public class CurrencyService {

	public List<List<String>> getCurrency(String dateStart, String dateFinish, String currencyCode){
		List<List<String>> listMy = new ArrayList<>();
		List<String> dateList = new ArrayList<String>();
		List<String> valueList = new ArrayList<String>();

		try {
			String url = "https://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=" 
						+ dateStart + "&date_req2=" + dateFinish + "&VAL_NM_RQ=" + currencyCode;
			HttpClient client = HttpClient.newBuilder().build();
        	HttpRequest request = HttpRequest.newBuilder()
							.uri(URI.create(url))						
							.build();
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.statusCode());
			//System.out.println(response.body());

			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(response.body()));
			Document doc = db.parse(is);
			NodeList records = doc.getElementsByTagName("Record");
			NodeList values = doc.getElementsByTagName("Value");
			//System.out.println(records.getLength());
			System.out.println(values.getLength());

			for (int i = 0; i < records.getLength(); i++) {
				Element element = (Element) values.item(i);
				String name = element.getTextContent();
				valueList.add(name);

				Element element2 = (Element) records.item(i);
				String name2 = element2.getAttribute("Date");
				dateList.add(name2);
		  	}
			listMy.add(dateList);
			listMy.add(valueList);
		
		}
		catch  (IOException  | InterruptedException | ParserConfigurationException | SAXException e) {
			System.out.println(e);
		}
		//System.out.println(listMy);
		return listMy;
	}
	
}
