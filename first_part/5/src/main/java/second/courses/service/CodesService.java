package second.courses.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

@Service
public class CodesService {

	public List<List<String>> getCodes(){
		List<List<String>> listMy = new ArrayList<>();
		List<String> nameList = new ArrayList<String>();
		List<String> codeList = new ArrayList<String>(); 

		try {
			String url = "http://www.cbr.ru/scripts/XML_val.asp?d=0";
			HttpClient client = HttpClient.newBuilder().build();
        	HttpRequest request = HttpRequest.newBuilder()
							.uri(URI.create(url))						
							.build();
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.statusCode());

			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(response.body()));
			Document doc = db.parse(is);
			NodeList names = doc.getElementsByTagName("EngName");
			NodeList codes = doc.getElementsByTagName("ParentCode");
			//System.out.println(names.getLength());
			//System.out.println(codes.getLength());

			for (int i = 0; i < names.getLength(); i++) {
				Element element = (Element) names.item(i);
				String name = element.getTextContent();
				nameList.add(name);

				Element element2 = (Element) codes.item(i);
				String code = element2.getTextContent();
				codeList.add(code);
		  	}
			listMy.add(nameList);
			listMy.add(codeList);
		}
		catch  (IOException  | InterruptedException | ParserConfigurationException | SAXException e) {
			System.out.println(e);
		}

		//System.out.println(listMy);
		return listMy;

	}
	
}
