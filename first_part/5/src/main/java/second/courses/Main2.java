package second.courses;

import javafx.application.Application;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/*
2.	Реализовать приложение для построения графиков курса валют и/или биржевых курсов в режиме реального времени. 
Должна быть реализована возможность добавлять и удалять необходимые графики. 
 */

@SpringBootApplication
//@SpringBootApplication(scanBasePackages = {"com.example.crudjavafx"})
//@EnableJpaRepositories(basePackages = "second.courses.repository")
public class Main2 {

	public static void main(String[] args) {
		Application.launch(JavaFxApplication.class, args);

	}
	
}
