package third.third;

public class XmlFile extends BuilderWannabe{

	public XmlFile() {
        super();
    }

	@Override ManagerFile createRepresentation(Manager manager) {
		ManagerFile mf = new ManagerXmlFile();
		mf.append("id/" + manager.getId());
		mf.append("faxNumber/" + manager.getFaxNumber());
		mf.append("location/" + manager.getLocation());
		return mf;
	  }
	
}
