package third.third;

public class JsonFile extends BuilderWannabe{

		public JsonFile() {
        	super();
    }

	@Override ManagerFile createRepresentation(Manager manager) {
		ManagerFile mf = new ManagerJsonFile();
		mf.append("id:" + manager.getId());
		mf.append("faxNumber:" + manager.getFaxNumber());
		mf.append("location:" + manager.getLocation());
		return mf;
	  }
	
}
