package third.third;

public class FlatFile extends BuilderWannabe{

	public FlatFile() {
        super();
    }
	
	@Override ManagerFile createRepresentation(Manager manager) {
		ManagerFile mf = new ManagerFlatFile();
		mf.append("" + manager.getId() + EOL);
		mf.append(manager.getFaxNumber() + EOL);
		mf.append(manager.getLocation() + EOL);
		return mf;
	  }
}
