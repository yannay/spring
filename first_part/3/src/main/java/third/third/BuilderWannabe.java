package third.third;

public abstract class BuilderWannabe {
  protected static String EOL = System.getProperty("line.separator");

  abstract ManagerFile createRepresentation(Manager manager); 

  public static void main(String[] args) {
    Manager manager = new Manager(42);
    manager.setFaxNumber("303 867-5309");
    manager.setLocation("Gunnison");

    FlatFile bw = new FlatFile();
    ManagerFile flat = bw.createRepresentation(manager);
    PropertyFile bw2 = new PropertyFile();
    ManagerFile prop = bw2.createRepresentation(manager);
    JsonFile bw3 = new JsonFile();
    ManagerFile js = bw3.createRepresentation(manager);
    XmlFile bw4 = new XmlFile();
    ManagerFile xml = bw4.createRepresentation(manager);

    flat.write("manager.txt");
    prop.write("manager.properties");
    js.write("manager.json");
    xml.write("manager.xml");
    System.out.println("output files created");
  }
}
