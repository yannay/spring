// File: ManagerFile.java
package third.third;

public interface ManagerFile {
  public void append(String content);
  public void write(String filename);
}
