package third.third;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import org.w3c.dom.Document;

public class ManagerXmlFile implements ManagerFile{

	Document document;
	Element root;

	public ManagerXmlFile(){
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			document = documentBuilder.newDocument();
			root = document.createElement("manager");
            document.appendChild(root);

		}
		catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} 

	}
	

	public void append(String s) {
		int pos = s.indexOf("/");
		Element element = document.createElement(s.substring(0, pos));
		element.appendChild(document.createTextNode(s.substring(pos)));
		root.appendChild(element);

	}

	public void write(String path) {
		try (FileOutputStream output = new FileOutputStream(path)) {
					writeXml(document, output);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
    // write doc to output stream
    private static void writeXml(Document doc, OutputStream output) throws TransformerException {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);
        transformer.transform(source, result);

    }

	
}
