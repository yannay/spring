package third.third;

//import org.json.JSONObject;
import org.json.simple.JSONObject;
import java.io.FileWriter;
import java.io.IOException;

public class ManagerJsonFile implements ManagerFile{

	JSONObject obj = new JSONObject();

	public void append(String s) {
		int pos = s.indexOf(":");
		obj.put(s.substring(0, pos), s.substring(pos));
	}

	public void write(String path) {

		try (FileWriter file = new FileWriter(path)) {
            file.write(obj.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }

	}
	
}
