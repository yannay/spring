package third.third;

public class PropertyFile extends BuilderWannabe{

	public PropertyFile() {
        super();
    }
	
	@Override ManagerFile createRepresentation(Manager manager) {
		ManagerFile mf = new ManagerPropertyFile();
		mf.append("id=" + manager.getId());
		mf.append("faxNumber=" + manager.getFaxNumber());
		mf.append("location=" + manager.getLocation());
		return mf;
	  }
	
}
