package firstsecond;

import java.util.Properties;

public class IdGeneratorFabric { 

	// private static IdGenerator instance;
	
	// // Singleton for unic ID
	// public static synchronized IdGenerator getInstance() {
	// 	if (instance == null) {
	// 		instance = new IdGenerator();
	// 	}
	// 	return instance;
	// }

	private static volatile IdGeneratorFabric instance;
	private static Object mutex = new Object();

	protected IdGeneratorFabric() {
	}

	// Singleton for unic ID
	public static IdGeneratorFabric getInstance() {
		IdGeneratorFabric result = instance;
		if (result == null) {
			synchronized (mutex) {
				result = instance;
				if (result == null)
					//Properties prop = Prop.loadProp();
					if (Prop.existPropFile()){
						Properties prop = Prop.loadProp();
						int number = Integer.parseInt(prop.getProperty("number"));
						instance = result = new IdGeneratorProp(number);
					}
					else {
						instance = result = new IdGeneratorSimple();
					}
					//int number = Integer.parseInt(prop.getProperty("number"));
					//instance = result = new IdGenerator();
			}
		}
		return result;
	}

	private int counter = 0; 

	public int getNextId() { 
		return counter++;
	} 
}
	