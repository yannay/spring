package firstsecond;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.io.File;

public class Prop {

	private static String pathToFile = "firstsecond/config.properties";

	public static Properties loadProp(){
		Properties prop = new Properties();

		try (InputStream input = new FileInputStream(pathToFile)) {

            //Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            //System.out.println(prop.getProperty("number"));


        } catch (IOException ex) {
            ex.printStackTrace();
        }
		return prop;
	}

	public static Boolean existPropFile(){
		File file = new File(pathToFile);
		return file.exists() && !file.isDirectory();

	}
	
}
