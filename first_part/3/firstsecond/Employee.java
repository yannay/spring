package firstsecond;

public class Employee {

	private int id;
	private String firstName;
	private String lastName;
	private String email;

	public Employee(int id) {
		super();
		this.id = id;
	}

	public Employee(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
