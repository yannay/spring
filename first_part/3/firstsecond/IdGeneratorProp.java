package firstsecond;

public class IdGeneratorProp extends IdGeneratorFabric {

	private int counter;

	protected IdGeneratorProp(int counter) {
		this.counter = counter;
	}

	//private int counter = 0; 

	public int getNextId() { 
		return counter++; 
	} 
	
}
