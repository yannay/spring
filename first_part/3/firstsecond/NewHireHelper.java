package firstsecond;

class NewHireHelper extends Thread { 

	public void createNewEmployee() { 
		// Singleton for unic ID
		IdGeneratorFabric gen = IdGeneratorFabric.getInstance(); 
		int id = gen.getNextId();
		Employee e = new Employee(id);
		// add new employee to database
		System.out.println(e.getId());
		
	}
	public void run() { 
		createNewEmployee(); 
	} 
}