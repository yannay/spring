package fourth;

public class Main {

	public static void main(String[] args) {
		PanelDeveloper developer = new PanelDeveloper();
		developer.create();

		WoodDeveloper developer2 = new WoodDeveloper();
		developer2.create();
	}
	
}
