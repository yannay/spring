package com.example.crudjavafx.controller;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import com.fasterxml.jackson.databind.ObjectMapper;

public class EmployeeControllerTest {

	@Test
    public void getEmployees() throws Exception {
		HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/api/v1/employees")).build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		assertThat(response.statusCode()).isEqualTo(200);
	}

	@Test
	public void getEmployeeSuccess() throws Exception {
		HttpResponse<String> response = getEmployeeById("1");
		assertThat(response.statusCode()).isEqualTo(200);

		//System.out.println(response.body());
		JSONObject obj = new JSONObject(response.body());
		assertThat(obj.getString("firstName")).isEqualTo("Вячеслав");
		assertThat(obj.getString("lastName")).isEqualTo("Абрамов");
		assertThat(obj.getString("email")).isEqualTo("abramov1986@yandex.ru");
	}

	@Test
	public void getEmployeeNotFound() throws Exception {
		HttpResponse<String> response = getEmployeeById("399");
		assertThat(response.statusCode()).isEqualTo(404);
	}

	@Test
	public void updateDeleteGetEmployeeSuccess() throws Exception {
		String email = "makeev" + String.valueOf(ThreadLocalRandom.current().nextInt(0, 2000)) + "gmail.com";
		HttpResponse<String> response = updateEmployeeById("8", "Иван", "Макеев", email);
		assertThat(response.statusCode()).isEqualTo(200);

		//System.out.println(response.body());
		JSONObject obj = new JSONObject(response.body());
		assertThat(obj.getString("firstName")).isEqualTo("Иван");
		assertThat(obj.getString("lastName")).isEqualTo("Макеев");
		assertThat(obj.getString("email")).isEqualTo(email);
	}

	@Test
	public void createDeleteGetEmployeeSuccess() throws Exception {
		HttpResponse<String> response = createEmployee("Илья", "Бондарюк", "bondaruk1994@mail.ru");
		assertThat(response.statusCode()).isEqualTo(200);
		
		//System.out.println(response.body());
		JSONObject obj = new JSONObject(response.body());
		assertThat(obj.getString("firstName")).isEqualTo("Илья");
		assertThat(obj.getString("lastName")).isEqualTo("Бондарюк");
		assertThat(obj.getString("email")).isEqualTo("bondaruk1994@mail.ru");

		String id = obj.getString("id");
		HttpResponse<String> response2 = deleteEmployeeById(id);
		assertThat(response2.statusCode()).isEqualTo(200);
		//System.out.println(response.body());

		HttpResponse<String> response3 = getEmployeeById(id);
		assertThat(response3.statusCode()).isEqualTo(404);
	}


    private HttpResponse<String> getEmployeeById(String id) throws Exception {
		HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
								.uri(URI.create("http://localhost:8080/api/v1//employees/" + id))
								.build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response;
	}

	private HttpResponse<String> createEmployee(String firstName, String lastName, String email) throws Exception {
		var values = new HashMap<String, String>() {{
            put("firstName", firstName);
            put ("lastName", lastName);
			put ("email", email);
        }};

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(values);

		HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
								.uri(URI.create("http://localhost:8080/api/v1//employees/"))
								.POST(HttpRequest.BodyPublishers.ofString(requestBody))
								.header("Content-type", "application/json")
								.build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response;
	}

	private HttpResponse<String> deleteEmployeeById(String id) throws Exception {
		HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
								.uri(URI.create("http://localhost:8080/api/v1//employees/" + id))
								.DELETE()
								.build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response;
	}
	
	private HttpResponse<String> updateEmployeeById(String id, String firstName, String lastName, String email) throws Exception {
		var values = new HashMap<String, String>() {{
            put("firstName", firstName);
            put ("lastName", lastName);
			put ("email", email);
        }};

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(values);

		HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
								.uri(URI.create("http://localhost:8080/api/v1//employees/" + id))
								.PUT(HttpRequest.BodyPublishers.ofString(requestBody))
								.header("Content-type", "application/json")
								.build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response;
	}
}
