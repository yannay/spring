package com.example.crudjavafx.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.crudjavafx.controller.EmployeeController;
import com.example.crudjavafx.repository.EmployeeRepository;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

//import static org.junit.runner.RunWith;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {EmployeeController.class, EmployeeRepository.class})
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTests {
    // error EmployeeRepository available: expected at least 1 bean which qualifies as autowire candidate
	
	@Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeController employeeController;

    // @Test
    // public void testEmployeesTest() throws Exception {
    //     testEmployees();
    // }

	private void testEmployees() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employees")
                // .with(csrf())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultEmployees = result.getResponse().getContentAsString();
        assertNotNull(resultEmployees);
    }
	
}
