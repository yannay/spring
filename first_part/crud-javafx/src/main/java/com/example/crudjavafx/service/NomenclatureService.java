package com.example.crudjavafx.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.crudjavafx.model.Nomenclature;

@Service
// not used
public interface NomenclatureService {

	Nomenclature save(Nomenclature contact);

    public List<Nomenclature> findAll();
	
}
