package com.example.crudjavafx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.cell.PropertyValueFactory;

import net.rgielen.fxweaver.core.FxmlView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.crudjavafx.model.Nomenclature;
import com.example.crudjavafx.repository.NomenclatureRepository;
import com.example.crudjavafx.exception.ResourceNotFoundException;


@Component
@FxmlView("get_nomenclature.fxml")
public class GetNomenclatureController {

	private Stage stage;
	
	@FXML private TextField txtId;
	@FXML private VBox getNomenclature;
	@FXML private Button closeButton;
	@FXML public TableView<Nomenclature> nomenclatureTable;
    @FXML public TableColumn<Nomenclature, Integer> idColumn;
    @FXML public TableColumn<Nomenclature, String> nameColumn;
    @FXML public TableColumn<Nomenclature, String> artikulColumn;
    @FXML public TableColumn<Nomenclature, Boolean> serviceColumn;

	@Autowired
	private NomenclatureRepository nomenclatureRepository;

	private ObservableList<Nomenclature> data;

	@FXML
	public void initialize() {
		this.stage = new Stage();
        stage.setScene(new Scene(getNomenclature));

		data = FXCollections.observableArrayList();

		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        artikulColumn.setCellValueFactory(new PropertyValueFactory<>("artikul"));
        serviceColumn.setCellValueFactory(new PropertyValueFactory<>("service"));

		nomenclatureTable.getColumns().setAll(idColumn, nameColumn, artikulColumn, serviceColumn);
		nomenclatureTable.setItems(data);

	}

	public void show() {
		//System.out.println(data);
        stage.show();
    }

	@FXML
    public void getNomenclature() {
        String id = txtId.getText();
		data.clear();
		try {
			long id_long = Long.parseLong(id);
			Nomenclature nomenclature = nomenclatureRepository.findById(id_long)
			.orElseThrow(() -> new ResourceNotFoundException("Nomenclature not exist with id:" + id));
			data.add(nomenclature);
		}
		catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			//return;
		}
		catch (Exception e) {
			System.out.println("Wrong id field format");
			//return;
		}
		finally {
			// чистим поля
			txtId.setText("");
		}

    }
	
}