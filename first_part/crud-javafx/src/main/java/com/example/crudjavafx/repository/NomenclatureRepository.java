package com.example.crudjavafx.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;

import com.example.crudjavafx.model.Nomenclature;

@Repository
public interface NomenclatureRepository extends JpaRepository<Nomenclature, Long>{

	// public static ObservableList<Nomenclature> nomenclatures;

	// public static ObservableList<Nomenclature> getNomenclatures() {
	// 	//ObservableList<Nomenclature> nomenclatures;
    //     return FXCollections.unmodifiableObservableList(nomenclatures);
    // }
	List<Nomenclature> findAll();


}