package com.example.crudjavafx.controller;

import net.rgielen.fxweaver.core.FxmlView;
import net.rgielen.fxweaver.core.FxControllerAndView;

import org.springframework.stereotype.Component;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;


@Component
@FxmlView("main-stage.fxml")
public class MainController {

	private final FxControllerAndView<NomenclatureController, VBox> nomenclaturesDialog;

	@FXML
	private Label mainMenuLabel;

	@FXML
    public Button openNomeclaturesButton;

	//@Autowired
    public MainController(FxControllerAndView<NomenclatureController, VBox> nomenclaturesDialog) {
		this.nomenclaturesDialog = nomenclaturesDialog;
        
    }

	@FXML
	public void initialize() {
		openNomeclaturesButton.setOnAction(
                actionEvent -> nomenclaturesDialog.getController().show()
        );
	}

	// @FXML
	// public void showNomenclature(ActionEvent actionEvent) throws IOException{
	// 	JavaFxApplication.setRoot("show_nomenclature");
	// 	//JavaFxApplication.setRoot2(NomenclatureController.class);
	// 	//fxWeaver.loadController(NomenclatureController.class).show();
	// }

}