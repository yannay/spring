package com.example.crudjavafx.controller;

import net.rgielen.fxweaver.core.FxmlView;
import net.rgielen.fxweaver.core.FxControllerAndView;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.Scene;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.crudjavafx.model.Nomenclature;
import com.example.crudjavafx.repository.NomenclatureRepository;

@Component
@FxmlView("show_nomenclatures.fxml")
public class NomenclatureController {

	private Stage stage;
	private final FxControllerAndView<AddNomenclatureController, VBox> addNomenclatureDialog;
	private final FxControllerAndView<DeleteNomenclatureController, VBox> deleteNomenclatureDialog;
	private final FxControllerAndView<GetNomenclatureController, VBox> getNomenclatureDialog;
	private final FxControllerAndView<UpdateNomenclatureController, VBox> updateNomenclatureDialog;

	@Autowired
	private NomenclatureRepository nomenclatureRepository;

	//@Autowired
	//private NomenclatureService nomenclatureService;
	//@Autowired private NomenclatureRepository nomenclatureRepository;

	//@Autowired
	//public NomenclatureController(NomenclatureService nomenclatureService){
		//this.nomenclatureService = nomenclatureService;
	//}

	@FXML private VBox nomenclatures;
	@FXML private Button closeButton;
	@FXML private Button openAddNomeclatureButton;
	@FXML private Button openDeleteNomeclatureButton;
	@FXML private Button openGetNomeclatureButton;
	@FXML private Button openUpdateNomeclatureButton;
	@FXML public TableView<Nomenclature> nomenclatureTable;
    @FXML public TableColumn<Nomenclature, Integer> idColumn;
    @FXML public TableColumn<Nomenclature, String> nameColumn;
    @FXML public TableColumn<Nomenclature, String> artikulColumn;
    @FXML public TableColumn<Nomenclature, Boolean> serviceColumn;
	@FXML private TextField txtName;
    @FXML private TextField txtArtikul;
    @FXML private CheckBox boolService;

	private ObservableList<Nomenclature> data;

	public NomenclatureController(FxControllerAndView<AddNomenclatureController, VBox> addNomenclatureDialog,
									FxControllerAndView<DeleteNomenclatureController, VBox> deleteNomenclatureDialog,
									FxControllerAndView<GetNomenclatureController, VBox> getNomenclatureDialog,
									FxControllerAndView<UpdateNomenclatureController, VBox> updateNomenclatureDialog) {
		this.addNomenclatureDialog = addNomenclatureDialog;
        this.deleteNomenclatureDialog = deleteNomenclatureDialog;
		this.getNomenclatureDialog = getNomenclatureDialog;
		this.updateNomenclatureDialog = updateNomenclatureDialog;
    }

	@FXML
	public void initialize() {
		this.stage = new Stage();
        stage.setScene(new Scene(nomenclatures));

		openAddNomeclatureButton.setOnAction(
                actionEvent -> addNomenclatureDialog.getController().show(data)
        );

		openDeleteNomeclatureButton.setOnAction(
			//actionEvent -> System.out.println("345")
			actionEvent -> deleteNomenclatureDialog.getController().show(data)
		);

		openGetNomeclatureButton.setOnAction(
			//actionEvent -> System.out.println("345")
			actionEvent -> getNomenclatureDialog.getController().show()
		);

		openUpdateNomeclatureButton.setOnAction(
			//actionEvent -> System.out.println("345")
			actionEvent -> updateNomenclatureDialog.getController().show(data)
		);

        // closeButton.setOnAction(
        //         actionEvent -> stage.close()
        // );

		//System.out.println(nomenclatureRepository);

        //editButton.disableProperty().bind(Bindings.isEmpty(exampleTable.getSelectionModel().getSelectedItems()));
        //deleteButton.disableProperty().bind(Bindings.isEmpty(exampleTable.getSelectionModel().getSelectedItems()));

        // Добавляем столбцы к таблице
		List<Nomenclature> nomenclatures = nomenclatureRepository.findAll();
		data = FXCollections.observableArrayList(nomenclatures);

		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        artikulColumn.setCellValueFactory(new PropertyValueFactory<>("artikul"));
        serviceColumn.setCellValueFactory(new PropertyValueFactory<>("service"));

        //TableColumn<Nomenclature, Integer> idColumn = new TableColumn<>("ID");
        //idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));


        nomenclatureTable.getColumns().setAll(idColumn, nameColumn, artikulColumn, serviceColumn);

        // Добавляем данные в таблицу
        nomenclatureTable.setItems(data);

    }

	public void show() {
        stage.show();
    }


	// @FXML
    // private void switchToMain() throws IOException {
    //     JavaFxApplication.setRoot("main-stage");
    // }

	// @FXML
    // private void showN(ActionEvent actionEvent) {
    //    	//System.out.println(nomenclatureService);
	//    	System.out.println(nomenclatureRepository);
	//    	List<Nomenclature> nomenclatureList = nomenclatureRepository.findAll();
	// 	System.out.println(Arrays.toString(nomenclatureList.toArray()));
    // }
	
}
