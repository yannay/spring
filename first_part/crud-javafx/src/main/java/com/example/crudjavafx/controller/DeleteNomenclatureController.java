package com.example.crudjavafx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.collections.ObservableList;

import net.rgielen.fxweaver.core.FxmlView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.crudjavafx.model.Nomenclature;
import com.example.crudjavafx.repository.NomenclatureRepository;
import com.example.crudjavafx.exception.ResourceNotFoundException;


@Component
@FxmlView("delete_nomenclature.fxml")
public class DeleteNomenclatureController {

	private Stage stage;
	
	@FXML private TextField txtId;
	@FXML private VBox deleteNomenclature;
	@FXML private Button closeButton;

	@Autowired
	private NomenclatureRepository nomenclatureRepository;

	private ObservableList<Nomenclature> data;

	@FXML
	public void initialize() {
		this.stage = new Stage();
        stage.setScene(new Scene(deleteNomenclature));

		// closeButton.setOnAction(
		// 	actionEvent -> stage.close()
		// );
	}

	public void show(ObservableList<Nomenclature> data_new) {
		//System.out.println(data);
		data = data_new;
        stage.show();
    }

	@FXML
    public void deleteNomenclature() {
        String id = txtId.getText();
		try {
			long id_long = Long.parseLong(id);
			Nomenclature nomenclature = nomenclatureRepository.findById(id_long)
			.orElseThrow(() -> new ResourceNotFoundException("Nomenclature not exist with id:" + id));
			//System.out.println(nomenclature);
			for (Nomenclature n : data) {
				if (n.getId() == id_long){
					//System.out.println(id_long);
					data.remove(n);
					break;
				}
			}
			nomenclatureRepository.delete(nomenclature);
		}
		catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			//return;
		}
		catch (Exception e) {
			System.out.println("Wrong id field format");
			//return;
		}
		finally {
			// чистим поля
			txtId.setText("");
		}

    }
	
}
