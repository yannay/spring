package com.example.crudjavafx.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "nomenclature")
public class Nomenclature {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "name")
	private String name;

	@Column(name = "artikul")
	private String artikul;
	
	@Column(name = "service")
	private Boolean service;
	
	public Nomenclature() {
		
	}
	
	public Nomenclature(String name, String artikul, Boolean service) {
		super();
		this.name = name;
		this.artikul = artikul;
		this.service = service;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getArtikul() {
		return artikul;
	}
	public void setArtikul(String artikul) {
		this.artikul = artikul;
	}
	public Boolean getService() {
		return service;
	}
	public void setService(Boolean service) {
		this.service = service;
	}
}