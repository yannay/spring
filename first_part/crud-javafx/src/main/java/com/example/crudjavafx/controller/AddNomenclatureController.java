package com.example.crudjavafx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.collections.ObservableList;

import net.rgielen.fxweaver.core.FxmlView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;

import com.example.crudjavafx.model.Nomenclature;
import com.example.crudjavafx.repository.NomenclatureRepository;


@Component
@FxmlView("add_nomenclature.fxml")
public class AddNomenclatureController {

	private Stage stage;
	
	@FXML private TextField txtName;
    @FXML private TextField txtArtikul;
    @FXML private CheckBox boolService;
	@FXML private VBox addNomenclature;
	@FXML private Button closeButton;

	@Autowired
	private NomenclatureRepository nomenclatureRepository;

	private ObservableList<Nomenclature> data;

	@FXML
	public void initialize() {
		this.stage = new Stage();
        stage.setScene(new Scene(addNomenclature));

		// closeButton.setOnAction(
		// 	actionEvent -> stage.close()
		// );
	}

	public void show(ObservableList<Nomenclature> data_new) {
		//System.out.println(data);
		data = data_new;
        stage.show();
    }

	@FXML
    public void addNomenclature() {
        String name = txtName.getText();
        String artikul = txtArtikul.getText();
        Boolean service = boolService.isSelected();
        if (!StringUtils.hasLength(name) || !StringUtils.hasLength(artikul)) {
			System.out.println("Empty fields");
            return;
        }

        Nomenclature nomenclature = new Nomenclature(name, artikul, service);
        nomenclatureRepository.save(nomenclature);
        data.add(nomenclature);
		//System.out.println(data);

        // чистим поля
        txtName.setText("");
        txtArtikul.setText("");
        //txtService.set
    }
	
}
