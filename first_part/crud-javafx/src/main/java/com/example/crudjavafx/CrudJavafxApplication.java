package com.example.crudjavafx;

import javafx.application.Application;

import java.io.IOException;
import java.util.Properties;
import java.io.InputStream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
//@SpringBootApplication(scanBasePackages = {"com.example.crudjavafx"})
@EnableJpaRepositories(basePackages = "com.example.crudjavafx.repository")
public class CrudJavafxApplication {

	public static void main(String[] args) throws IOException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("my.properties");
		Properties p = new Properties();
		p.load(is);
		String version = p.getProperty("version");
		System.out.println("Version: " + version);
		if (version.equals("ui")){
			Application.launch(JavaFxApplication.class, args);
		}
		else {
			SpringApplication.run(CrudJavafxApplication.class, args);
		}

	}

}


