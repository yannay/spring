package edu.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import java.util.Arrays;

import javax.sql.DataSource;

import edu.spring.security.security.MyUserDetailsService;

@EnableWebSecurity
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    // TODO: add user details bean
    @Bean
    // public UserDetailsService userDetailsService() { 
    //     MyUserDetailsService userDetailsService = new MyUserDetailsService(); 
    //     userDetailsService.setUsername("user"); 
    //     userDetailsService.setPassword("123"); 
    //     userDetailsService.setRoles(Arrays.asList("ROLE_IT_USER", "ROLE_FIN_USER")); 
    //     return userDetailsService; 
    // } 
    public UserDetailsService userDetailsService(DataSource dataSource) { 
        JdbcDaoImpl jdbcDao = new JdbcDaoImpl(); 
        jdbcDao.setDataSource(dataSource); 
        return jdbcDao; 
    } 
        


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/public/*.*")
                .antMatchers("/h2-console/**/*")
                .antMatchers("/index.jsp")
                .antMatchers("/style.css");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/**/*.*")
                .access("hasRole('ROLE_FIN_USER') or hasRole('ROLE_IT_USER')")
                .and()
                .httpBasic();
                
    }
}
