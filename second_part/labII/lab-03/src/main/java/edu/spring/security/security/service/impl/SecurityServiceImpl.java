package edu.spring.security.security.service.impl;

import edu.spring.security.domain.entity.DomainEntity;
import edu.spring.security.security.dao.SecurityObjectDao;
import edu.spring.security.security.objects.PermissionObject;
import edu.spring.security.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final SecurityObjectDao securityObjectDao;

    @Autowired
    public SecurityServiceImpl(SecurityObjectDao securityObjectDao) {
        this.securityObjectDao = securityObjectDao;
    }

    @Transactional
    public void grantPermissions(DomainEntity entity, PermissionObject po) {
        //todo - To be implemented ...
    }

    @Transactional
    public void removeACL(DomainEntity entity) {
        //todo - To be implemented ...
    }

    @Transactional
    public Collection<PermissionObject> getPermissions(DomainEntity entity) {
        //todo - To be implemented ...
        return Collections.emptyList();
    }

    @Transactional
    public Collection<String> getRoles() {
        return securityObjectDao.getRoles();
    }

    @Transactional
    public Collection<String> getUsernames() {
        return securityObjectDao.getUsernames();
    }

    @Transactional
    public void initDefaultACL(DomainEntity entity) {
        //todo - To be implemented ...
    }
}
