package edu.spring.security.domain.dao.impl;

import edu.spring.security.domain.dao.ContactDao;
import edu.spring.security.domain.entity.Contact;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Repository
public class ContactDaoImpl implements ContactDao {

    @PersistenceContext
    private EntityManager entityManager;

    public Collection<Contact> getAllContacts() {
        return entityManager.createQuery("select c from Contact c", Contact.class).getResultList();
    }

    public Contact getContact(long id) {
        return entityManager.find(Contact.class, id);
    }

    public void save(Contact person) {
        entityManager.persist(person);
    }

    public void remove(Contact person) {
        entityManager.remove(person);
    }
}
