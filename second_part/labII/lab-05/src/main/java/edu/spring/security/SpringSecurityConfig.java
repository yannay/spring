package edu.spring.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Bean
    public UserDetailsService userDetailsService(DataSource dataSource) {
        JdbcDaoImpl jdbcDao = new JdbcDaoImpl();
        jdbcDao.setDataSource(dataSource);
        return jdbcDao;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // here is nothing to ignore
        // web.ignoring()
        //     .antMatchers("/public/*.*")
        //     .antMatchers("/h2-console/**/*")
        //     .antMatchers("/index.jsp")
        //     .antMatchers("/style.css");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                // TODO: mappings
                .antMatchers("/public/*.*", "/h2-console/**/*", "/index.jsp", "/style.css") 
                .access("hasRole('ROLE_ANONYMOUS')")
                .antMatchers("/**/*.*")
                .access("hasRole('ROLE_FIN_USER') or hasRole('ROLE_IT_USER') ")
                .and()
                .formLogin()
                .loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .loginPage("/public/login.jsp")
                .failureForwardUrl("/public/noaccess.jsp")
                .successForwardUrl("/contact/list.do")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/public/login.jsp")
                .logoutUrl("/j_spring_security_logout")
                // TODO: anonymous authorization
                .and() 
                .anonymous()
                .authorities("ROLE_ANONYMOUS")
                // .authorities("ROLE_ANONYMOUS", "ROLE_FIN_USER")
                .principal("anonymous")
                ;
    }
}
