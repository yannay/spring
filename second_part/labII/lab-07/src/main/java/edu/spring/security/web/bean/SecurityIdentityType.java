package edu.spring.security.web.bean;

public enum SecurityIdentityType {
    Role, User
}
