package edu.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.context.request.RequestContextHolder;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Bean
    public UserDetailsService userDetailsService(DataSource dataSource) {
        JdbcDaoImpl jdbcDao = new JdbcDaoImpl();
        jdbcDao.setDataSource(dataSource);
        return jdbcDao;
    }

    // @Bean
    // public SessionRegistry sessionRegistry() {
    //     SessionRegistry sessionRegistry = new SessionRegistryImpl();
    //     return sessionRegistry;
    // }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/public/*.*")
                .antMatchers("/h2-console/**/*")
                .antMatchers("/index.jsp")
                .antMatchers("/style.css");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/**/*.*")
                .access("hasRole('ROLE_FIN_USER') or hasRole('ROLE_IT_USER') ")
                .and()
                .formLogin()
                .loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .loginPage("/public/login.jsp")
                .failureForwardUrl("/public/noaccess.jsp")
                .successForwardUrl("/contact/list.do")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/public/noaccess.jsp")
                .logoutUrl("/j_spring_security_logout")
                // .clearAuthentication(true)
                // .addLogoutHandler((request, response, auth) -> {
                //     System.out.println(RequestContextHolder.currentRequestAttributes().getSessionId());
                //     System.out.println("logout handler");
                //     for (Cookie cookie : request.getCookies()) {
                //         String cookieName = cookie.getName();
                //         Cookie cookieToDelete = new Cookie(cookieName, null);
                //         cookieToDelete.setMaxAge(0);
                //         response.addCookie(cookieToDelete);
                //     }
                // })
                // TODO: session management
                .and()
                .sessionManagement() 
                .sessionFixation()
                .newSession()
                //.none()
                //.migrateSession()
                .maximumSessions(1) 
                .maxSessionsPreventsLogin(false)
                //.sessionRegistry(sessionRegistry())
                ;
    }
}
