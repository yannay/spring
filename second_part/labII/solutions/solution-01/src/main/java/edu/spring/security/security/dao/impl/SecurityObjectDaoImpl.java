package edu.spring.security.security.dao.impl;

import edu.spring.security.security.dao.SecurityObjectDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

@Repository
public class SecurityObjectDaoImpl implements SecurityObjectDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SecurityObjectDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Collection<String> getRoles() {
        return jdbcTemplate
                .query("select distinct authority from authorities order by authority", new StringRowMapper());
    }

    public Collection<String> getUsernames() {
        return jdbcTemplate
                .query("select username from users where enabled = 1 order by username", new StringRowMapper());
    }

    private class StringRowMapper implements RowMapper<String> {
        public String mapRow(ResultSet rs, int i) throws SQLException {
            return rs.getString(1);
        }
    }
}
