package edu.spring.security.domain.service;

import edu.spring.security.domain.entity.Contact;

import java.util.Collection;

public interface ContactService {
    Collection<Contact> getAllContacts();

    Contact getContact(long id);

    void save(Contact contact);

    void remove(Contact contact);

    void create(Contact contact);
}
