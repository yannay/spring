package edu.spring.security.domain.service.impl;

import edu.spring.security.domain.dao.ContactDao;
import edu.spring.security.domain.entity.Contact;
import edu.spring.security.domain.service.ContactService;
import edu.spring.security.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class ContactServiceImpl implements ContactService {

    private final ContactDao personDao;
    private final SecurityService securityService;

    @Autowired
    public ContactServiceImpl(ContactDao personDao, SecurityService securityService) {
        this.personDao = personDao;
        this.securityService = securityService;
    }


    @Transactional
    public Collection<Contact> getAllContacts() {
        return personDao.getAllContacts();
    }

    @Transactional
    public Contact getContact(long id) {
        return personDao.getContact(id);
    }

    @Transactional
    public void create(Contact contact) {
        personDao.save(contact);
        securityService.initDefaultACL(contact);
    }

    @Transactional
    public void save(Contact contact) {
        personDao.save(contact);
    }

    @Transactional
    public void remove(Contact contact) {
        securityService.removeACL(contact);
        personDao.remove(contact);
    }
}
