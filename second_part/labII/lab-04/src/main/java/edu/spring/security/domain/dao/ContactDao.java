package edu.spring.security.domain.dao;

import edu.spring.security.domain.entity.Contact;

import java.util.Collection;


public interface ContactDao {

    Collection<Contact> getAllContacts();

    Contact getContact(long id);

    void save(Contact contact);

    void remove(Contact contact);
}
