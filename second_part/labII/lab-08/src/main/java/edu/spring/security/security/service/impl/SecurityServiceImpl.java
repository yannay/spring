package edu.spring.security.security.service.impl;

import edu.spring.security.domain.entity.DomainEntity;
import edu.spring.security.security.dao.SecurityObjectDao;
import edu.spring.security.security.objects.PermissionObject;
import edu.spring.security.security.objects.SecurityIdentType;
import edu.spring.security.security.service.SecurityService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final SecurityObjectDao securityObjectDao;
    private final MutableAclService aclService;
    
    private static final Logger logger = Logger.getLogger(SecurityServiceImpl.class);

    @Autowired
    public SecurityServiceImpl(MutableAclService aclService, SecurityObjectDao securityObjectDao) { 
        this.aclService = aclService; 
        this.securityObjectDao = securityObjectDao; 
    } 

    // @Autowired
    // public SecurityServiceImpl(SecurityObjectDao securityObjectDao) {
    //     this.securityObjectDao = securityObjectDao;
    // }

    private static final Permission[] OWNER_PERMISSIONS = { 
        BasePermission.CREATE, BasePermission.READ, BasePermission.WRITE, BasePermission.DELETE 
    };
        

    @Transactional
    public void grantPermissions(DomainEntity entity, PermissionObject po) {
        //todo - To be implemented ...
        MutableAcl acl = readAcl(entity); 
        if (acl == null) {
            acl = aclService.createAcl(new ObjectIdentityImpl(entity.getClass(), entity.getId()));
            acl.setOwner(new PrincipalSid( SecurityContextHolder.getContext().getAuthentication()));
        } 
        acl.insertAce(acl.getEntries().size(), po.getPermission(), createSid(po), true); 
        aclService.updateAcl(acl);
    }

    private static Sid createSid(PermissionObject po) {
        if (po.getSecurityIdentType() == SecurityIdentType.Authority) {
            return new GrantedAuthoritySid(po.getSecurityIdentName());
        } else if (po.getSecurityIdentType() == SecurityIdentType.Principal) {
            return new PrincipalSid(po.getSecurityIdentName()); } 
        else {
            throw new RuntimeException("Unsupported security identity type."); }
        }

    @Transactional
    public void removeACL(DomainEntity entity) {
        //todo - To be implemented ...
        aclService.deleteAcl(new ObjectIdentityImpl(entity.getClass(), entity.getId()), true);
    }

    @Transactional
    public Collection<PermissionObject> getPermissions(DomainEntity entity) {
        //todo - To be implemented ...
        //return Collections.emptyList();
        Collection<PermissionObject> result = new ArrayList<PermissionObject>();
        Acl acl = readAcl(entity); 
        if (acl == null) {
            return Collections.emptyList(); 
        }
        for (AccessControlEntry entry : acl.getEntries()) { 
            PermissionObject po = createPermissionObject(entry); 
            po.setDomainEntityId(entity.getId()); 
            result.add(po);
        }
        return result;
    }

    private MutableAcl readAcl(DomainEntity entity) { try {
        return (MutableAcl) aclService.readAclById(
        new ObjectIdentityImpl(entity.getClass(), entity.getId()));
        } catch (Exception e) { logger.error(e);
                return null;
            }
    }

    private static PermissionObject createPermissionObject(AccessControlEntry entry) { PermissionObject result = new PermissionObject(); result.setPermission(entry.getPermission());
        if (entry.getSid() instanceof GrantedAuthoritySid) {
            GrantedAuthoritySid sid = (GrantedAuthoritySid) entry.getSid(); 
            result.setSecurityIdentName((sid).getGrantedAuthority()); 
            result.setSecurityIdentType(SecurityIdentType.Authority);
        } else if (entry.getSid() instanceof PrincipalSid) { 
            PrincipalSid sid = (PrincipalSid) entry.getSid(); 
            result.setSecurityIdentName(sid.getPrincipal()); 
            result.setSecurityIdentType(SecurityIdentType.Principal);
        } else {
            throw new RuntimeException("Unsupported type of Sid.");
        }
        return result;
    }

    @Transactional
    public Collection<String> getRoles() {
        return securityObjectDao.getRoles();
    }

    @Transactional
    public Collection<String> getUsernames() {
        return securityObjectDao.getUsernames();
    }

    @Transactional
    public void initDefaultACL(DomainEntity entity) {
        //todo - To be implemented ...
        removeACL(entity); 
        Sid owner = new PrincipalSid( SecurityContextHolder.getContext().getAuthentication()); 
        MutableAcl acl = aclService.createAcl(new ObjectIdentityImpl(entity.getClass(), entity.getId())); 
        acl.setOwner(owner); 
        for (Permission permission : OWNER_PERMISSIONS) { 
            acl.insertAce(acl.getEntries().size(), permission, owner, true); 
        } 
        aclService.updateAcl(acl); 
    }
}
