package edu.spring.security.security.objects;

public enum SecurityIdentType {
    Principal,
    Authority
}
