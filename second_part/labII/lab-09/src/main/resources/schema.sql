drop table if exists authorities;
drop table if exists users;

create table users (
    username        varchar(50) not null primary key,
    password        varchar(50) not null,
    enabled         boolean not null
);


create table authorities (
    username        varchar(50) not null,
    authority       varchar(50) not null,
    foreign key (username) references users (username)
);

drop table if exists acl_entry;
drop table if exists acl_object_identity;
drop table if exists acl_class;
drop table if exists acl_sid;

create table acl_sid (
    id                      bigint not null auto_increment primary key,
    principal               tinyint(1) not null,
    sid                     varchar(100) not null,
);


create table acl_class (
    id                      bigint not null auto_increment primary key,
    class                   varchar(100) unique not null
);


create table acl_object_identity (
    id                      bigint not null auto_increment primary key,
    object_id_class         bigint not null,
    object_id_identity      bigint not null,
    parent_object           bigint,
    owner_sid               bigint,
    entries_inheriting      tinyint(1) not null,

    foreign key (object_id_class) references acl_class (id),
    foreign key (parent_object) references acl_object_identity (id),
    foreign key (owner_sid) references acl_sid (id)
);


create table acl_entry (
    id                      bigint not null auto_increment primary key,
    acl_object_identity     bigint not null,
    ace_order               int not null,
    sid                     bigint not null,
    mask                    int not null,
    granting                tinyint(1) not null,
    audit_success           tinyint(1) not null,
    audit_failure           tinyint(1) not null,

    foreign key (acl_object_identity) references acl_object_identity (id),
    foreign key (sid) references acl_sid (id)
);
