package edu.spring.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.security.acls.model.Permission;

import edu.spring.security.domain.entity.DomainEntity;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

	@Autowired
    private MutableAclService aclService;

	private static final Map<String, Permission> permissionMap = new HashMap<>();
    static {
        permissionMap.put("CREATE", BasePermission.CREATE);
        permissionMap.put("READ", BasePermission.READ);
        permissionMap.put("WRITE", BasePermission.WRITE);
        permissionMap.put("DELETE", BasePermission.DELETE);
        permissionMap.put("ADMIN", BasePermission.ADMINISTRATION);
    }

	@Override
	public boolean hasPermission(Authentication authentication, Object entity, Object permission) {
		//System.out.println(authentication);
		// for (GrantedAuthority grantedAuth : authentication.getAuthorities()) {
		// 	System.out.println(grantedAuth.getAuthority());
		// }

		if (authentication != null && entity instanceof DomainEntity && permission != null) {
			//System.out.println(BasePermission.READ);
			return getPermissions((DomainEntity) entity, authentication.getName()).contains(permissionMap.get(permission));
		}
		return false;
	}


	@Override
	public boolean hasPermission(Authentication authentication, Serializable serializable, String targetType,
			Object permission) {
		return false;
	}

	private Collection<Permission> getPermissions(DomainEntity entity, String userName) {
        Collection<Permission> result = new ArrayList<Permission>();

        Acl acl = readAcl(entity);
        if (acl == null) {
            return Collections.emptyList();
        }

        for (AccessControlEntry entry : acl.getEntries()) {
            Permission po = entry.getPermission();
			if (entry.getSid().toString().contains(userName)){
				//System.out.println(entry.getSid().toString());
            	result.add(po);
			}
        }

        return result;
    }

	private MutableAcl readAcl(DomainEntity entity) {
        try {
            return (MutableAcl) aclService.readAclById(
                    new ObjectIdentityImpl(entity.getClass(), entity.getId()));
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            return null;
        }
    }
	
}
