package edu.spring.security;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.acls.domain.*;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.jdbc.LookupStrategy;
import org.springframework.security.acls.model.AclCache;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.PermissionGrantingStrategy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Bean
    public UserDetailsService userDetailsService(DataSource dataSource) {
        JdbcDaoImpl jdbcDao = new JdbcDaoImpl();
        jdbcDao.setDataSource(dataSource);
        return jdbcDao;
    }

    @Bean
    public MutableAclService aclService(
            DataSource dataSource, LookupStrategy lookupStrategy,
            AclCache aclCache
    ) {
        return new JdbcMutableAclService(dataSource, lookupStrategy, aclCache);
    }

    @Bean
    public LookupStrategy lookupStrategy(
            DataSource dataSource, AclCache aclCache,
            AclAuthorizationStrategy aclAuthorizationStrategy,
            AuditLogger auditLogger
    ) {
        return new BasicLookupStrategy(
                dataSource, aclCache, aclAuthorizationStrategy, auditLogger
        );
    }

    @Bean
    public AclCache aclCache(
            PermissionGrantingStrategy permissionGrantingStrategy,
            AclAuthorizationStrategy aclAuthorizationStrategy
    ) {
        net.sf.ehcache.config.Configuration configuration =
                new net.sf.ehcache.config.Configuration();
        net.sf.ehcache.CacheManager manager =
                net.sf.ehcache.CacheManager.create(configuration);
        Cache aclCache = new Cache(
                new CacheConfiguration("aclCache", 1000));
        manager.removeAllCaches();
        aclCache.setName("aclCache");
        manager.addCache(aclCache);

        return new EhCacheBasedAclCache(
                aclCache,
                permissionGrantingStrategy,
                aclAuthorizationStrategy
        );
    }

    @Bean
    public AclAuthorizationStrategy aclAuthorizationStrategy() {
        return new AclAuthorizationStrategyImpl(
                new SimpleGrantedAuthority("ROLE_ADMIN")
        );
    }

    @Bean
    public PermissionGrantingStrategy permissionGrantingStrategy(
            AuditLogger auditLogger
    ) {
        return new DefaultPermissionGrantingStrategy(auditLogger);
    }

    @Bean
    public AuditLogger auditLogger() {
        return new ConsoleAuditLogger();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/public/*.*")
                .antMatchers("/h2-console/**/*")
                .antMatchers("/index.jsp")
                .antMatchers("/style.css");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/**/*.*")
                .access("hasRole('ROLE_FIN_USER') or hasRole('ROLE_IT_USER') ")
                .and()
                .formLogin()
                .loginProcessingUrl("/j_spring_security_check")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .loginPage("/public/login.jsp")
                .failureForwardUrl("/public/noaccess.jsp")
                .successForwardUrl("/contact/list.do")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/public/login.jsp")
                .logoutUrl("/j_spring_security_logout");
    }
}
