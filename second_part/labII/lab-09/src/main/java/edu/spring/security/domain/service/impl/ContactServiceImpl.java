package edu.spring.security.domain.service.impl;

import edu.spring.security.domain.dao.ContactDao;
import edu.spring.security.domain.entity.Contact;
import edu.spring.security.domain.service.ContactService;
import edu.spring.security.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class ContactServiceImpl implements ContactService {

    private final ContactDao personDao;
    private final SecurityService securityService;

    @Autowired
    public ContactServiceImpl(ContactDao personDao, SecurityService securityService) {
        this.personDao = personDao;
        this.securityService = securityService;
    }


    @Transactional
    @PostFilter("hasPermission(filterObject, 'READ')") 
    public Collection<Contact> getAllContacts() {
        // Collection<Contact> res = new ArrayList<Contact>();
        // System.out.println(personDao.getAllContacts().size());
        // for (Contact con : personDao.getAllContacts()){
        //     System.out.println(securityService.getPermissions(con).size());
        //     for (PermissionObject per : securityService.getPermissions(con)){
        //         if (per.getPermission() == BasePermission.READ) {
        //             System.out.println(per.getPermission());
        //             res.add(con);
        //         }
        //     }
        // }
        return personDao.getAllContacts();
        //return res;
    }

    @Transactional
    @PostAuthorize("hasPermission(returnObject, 'READ')") 
    public Contact getContact(long id) {
        return personDao.getContact(id);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_FIN_USER') || hasRole('ROLE_IT_USER')") 
    public void create(Contact contact) {
        personDao.save(contact);
        securityService.initDefaultACL(contact);
    }

    @Transactional
    @PreAuthorize("hasPermission(#contact, 'WRITE')") 
    public void save(Contact contact) {
        //contact
        personDao.save(contact);
    }

    @Transactional
    @PreAuthorize("hasPermission(#contact, 'DELETE')") 
    public void remove(Contact contact) {
        securityService.removeACL(contact);
        personDao.remove(contact);
    }
}
